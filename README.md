# Português
Por favor, verifique o arquivo readme, de acordo com sua língua

Português = readme-pt.md

# English
Please, check the readme file according to your language.

English = readme-en.md
